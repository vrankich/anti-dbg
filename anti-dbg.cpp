#include <iostream>
#include "functions.h"
#include "software_breakpoint.h"
#include "hardware_breakpoints.h"
#include "exception_handling.h"
#include "checksum.h"

PVOID function_to_check()
{
    std::cout << "Some  line...\n";
    return nullptr;
}

int main()
{
    //g_p_func = (PVOID)&function_to_check;
    g_p_func = (PVOID)function_to_check;
    g_dw_func_size = get_func_size(g_p_func);
    /* count original checksum of a function */
    g_dw_original_checksum = CRC32((uint8_t*)g_p_func, g_dw_func_size);    
    
    HANDLE functions_thread = CreateThread(NULL, NULL, run_check_with_functions, NULL, NULL, NULL);
    HANDLE checksum_thread = CreateThread(NULL, NULL, thread_func_CRC32, NULL, NULL, NULL);
    HANDLE exceptions_thread = CreateThread(NULL, NULL, check_exception_handling, NULL, NULL, NULL);
    HANDLE harware_bp_thread = CreateThread(NULL, NULL, check_hardware_breakpoints, NULL, NULL, NULL);

    function_to_check();

    auto func = function_to_check;
    if (check_software_breakpoint((PVOID)func)) {
        std::cout << "The program is being debugged... (software breakpoints)\n";
        ExitProcess(0);
    }
 
    g_thread_functions = false;
    g_thread_checksum = false;
    g_thread_exceptions = false;
    g_hardware_breakpoints = false;

    WaitForSingleObject(functions_thread, 10010);
    WaitForSingleObject(checksum_thread, 10010);
    WaitForSingleObject(exceptions_thread, 10010);
    WaitForSingleObject(harware_bp_thread, 10010);
    
    std::cout << "The program is not being debugged...\n";

	return 0;
}

