#include "checksum.h"


uint32_t CRC32(const uint8_t data[], size_t data_length) 
{
    uint32_t crc32 = 0xFFFFFFFFu;
    uint32_t CRCTable[256];

    for (size_t i = 0; i < data_length; i++) {
        const uint32_t lookupIndex = (crc32 ^ data[i]) & 0xff;
        crc32 = (crc32 >> 8) ^ CRCTable[lookupIndex];
    }

    crc32 ^= 0xFFFFFFFFu;
    return crc32;
}

DWORD WINAPI thread_func_CRC32(LPVOID lp_thread_parameter)
{
    while (g_thread_checksum)
    {
        /* count checksum of a func and compare with original */
        if (CRC32((uint8_t*)g_p_func, g_dw_func_size) != g_dw_original_checksum) {
            std::cout << "The program is being debugged (checksum)...\n";
            ExitProcess(0);
        }
        Sleep(10000);
    }
    return 0;
}

size_t get_func_size(PVOID ptr_func)
{
    PBYTE ptr = (PBYTE)ptr_func;
    size_t n = 0;
    
    do {
        n++;
    } while (*(ptr++) != 0xC3);
    
    return n;
}

