#pragma once

#include <windows.h>
#include <basetsd.h>
#include <zlib.h>
#include <inttypes.h>
#include <iostream>

inline bool g_thread_checksum = true;

/* globar vars for checksum */
inline PVOID g_p_func;
inline DWORD g_dw_func_size;
inline DWORD g_dw_original_checksum;

uint32_t CRC32(const uint8_t data[], size_t data_length);
DWORD WINAPI thread_func_CRC32(LPVOID lp_thread_parameter);
size_t get_func_size(PVOID ptr_func);