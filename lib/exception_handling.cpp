#include "exception_handling.h"

bool is_debugged = true;

/* Filter function will be called if the application is not being debugged */
LONG WINAPI unhandled_exception_filter(PEXCEPTION_POINTERS ptr_info)
{
    is_debugged = false;
    return EXCEPTION_CONTINUE_EXECUTION;
}

void exception_handler()
{
	PTOP_LEVEL_EXCEPTION_FILTER prev_filter = SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER)unhandled_exception_filter);
	/* Raising an exception causes the exception dispatcher to go through the search for an exception handler */
	RaiseException(EXCEPTION_FLT_DIVIDE_BY_ZERO, 0, 0, NULL);
	SetUnhandledExceptionFilter(prev_filter);
}


DWORD WINAPI check_exception_handling(LPVOID lp_thread_parameter)
{
	while (g_thread_exceptions) {
		exception_handler();
		if (is_debugged) {
			std::cout << "The program is being debugged (exception handling)...\n";
			ExitProcess(0);
		}
		Sleep(10000);
	}

	return 0;
} 

