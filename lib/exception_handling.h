#pragma once

#include <iostream>
#include <windows.h>

inline bool g_thread_exceptions = true;

DWORD WINAPI check_exception_handling(LPVOID lp_thread_parameter);