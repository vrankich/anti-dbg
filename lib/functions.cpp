#include "functions.h"

/* вручную проверить флаг BeingDebugged */
int is_being_debugged()
{
	#ifndef _WIN64
	/* указатель на PEB структуру */
	PPEB peb_ptr = (PPEB)__readfsdword(0x30);
	#else
	PPEB peb_ptr = (PPEB)__readgsqword(0x60);
	#endif
	 
	if (peb_ptr->BeingDebugged) {
		std::cout << "The program is being debugged (flag BeingDebugged)...\n";
		ExitProcess(0);
	}

	return 0;
}

int check_heap_flags()
{

#ifndef _WIN64
    PPEB pPeb = (PPEB)__readfsdword(0x30);
    PVOID pHeapBase = !m_bIsWow64
        ? (PVOID)(*(PDWORD_PTR)((PBYTE)pPeb + 0x18))
        : (PVOID)(*(PDWORD_PTR)((PBYTE)pPeb + 0x1030));
    DWORD dwHeapFlagsOffset = IsWindowsVistaOrGreater()
        ? 0x40
        : 0x0C;
    DWORD dwHeapForceFlagsOffset = IsWindowsVistaOrGreater()
        ? 0x44 
        : 0x10;
#else
    PPEB pPeb = (PPEB)__readgsqword(0x60);
    PVOID pHeapBase = (PVOID)(*(PDWORD_PTR)((PBYTE)pPeb + 0x30));
    DWORD dwHeapFlagsOffset = IsWindowsVistaOrGreater()
        ? 0x70 
        : 0x14;
    DWORD dwHeapForceFlagsOffset = IsWindowsVistaOrGreater()
        ? 0x74 
        : 0x18;
#endif

    PDWORD pdwHeapFlags = (PDWORD)((PBYTE)pHeapBase + dwHeapFlagsOffset);
    PDWORD pdwHeapForceFlags = (PDWORD)((PBYTE)pHeapBase + dwHeapForceFlagsOffset);
    
    if ((*pdwHeapFlags & ~HEAP_GROWABLE) || (*pdwHeapForceFlags != 0)) {
		std::cout << "The program is being debugged (heap flags)...\n";
		ExitProcess(0);
    }

    return 0;
}


int is_debugged_NtQuery() 
{
	HMODULE hNtdll = LoadLibraryA("ntdll.dll");
	
	if (hNtdll)
	{
	    auto pfnNtQueryInformationProcess = (TNtQueryInformationProcess)GetProcAddress(
	        hNtdll, "NtQueryInformationProcess");
	    
	    if (pfnNtQueryInformationProcess)
	    {
	        DWORD dwProcessDebugPort, dwReturned;
	        NTSTATUS status = pfnNtQueryInformationProcess(
	            GetCurrentProcess(),
	            ProcessDebugPort,
	            &dwProcessDebugPort,
	            sizeof(DWORD),
	            &dwReturned);

	        if (NT_SUCCESS(status) && (-1 == dwProcessDebugPort)) {
	        	std::cout << "The program is being debugged (NtQuery)...\n";
				ExitProcess(0);
	        }
	    }
	}

	return 0;
}

int check_NtGlobalFlag() 
{
#ifdef _WIN64
	DWORD pNtGlobalFlag = NULL;
	PPEB pPeb = (PPEB)__readgsqword(0x60);
	pNtGlobalFlag = *(PDWORD)((PBYTE)pPeb + 0xBC);
#else
	DWORD pNtGlobalFlag = NULL;
	PPEB pPeb = (PPEB)__readfsdword(0x30);
	pNtGlobalFlag = *(PDWORD)((PBYTE)pPeb + 0x68);
#endif

	if ((pNtGlobalFlag & 0x70) != 0) {
		std::cout << "The program is being debugged (NtGlobalFlag)...\n";
		ExitProcess(0);
	}

	return 0;
}

int is_debugger_present()
{
	HINSTANCE kern_lib = LoadLibraryEx("kernel32.dll", NULL, 0 );

	if (kern_lib) {
		FARPROC lIsDebuggerPresent = GetProcAddress( kern_lib, "IsDebuggerPresent" );
		if( lIsDebuggerPresent && lIsDebuggerPresent() ) {
			std::cout << "The program is being debugged (IsDebuggerPresent)...\n";
			ExitProcess(0);
		}
		FreeLibrary(kern_lib);
	}

	return 0;
 }

int is_task_man_running()
{
	HWND hwndTaskMan = FindWindowEx(NULL, NULL, NULL, "Task Manager");
	
	if (hwndTaskMan) {
		std::cout << "Task Manager is running...\n";
		ExitProcess(0); 
	}
	
	return 0;
}

DWORD WINAPI run_check_with_functions(LPVOID lp_thread_parameter)
{
	while (g_thread_functions) {
		is_being_debugged();
		check_heap_flags();
		is_debugged_NtQuery();
		check_NtGlobalFlag();
		is_task_man_running();
		is_debugger_present();
		Sleep(10000);
	}

	return 0;
}
