#pragma once

#include <iostream>
#include <windows.h>
#include <winternl.h>
#include <versionhelpers.h>

typedef NTSTATUS (NTAPI *TNtQueryInformationProcess)(
    IN HANDLE           ProcessHandle,
    IN PROCESSINFOCLASS ProcessInformationClass,
    OUT PVOID           ProcessInformation,
    IN ULONG            ProcessInformationLength,
    OUT PULONG          ReturnLength
);

inline bool g_thread_functions = false;

DWORD WINAPI run_check_with_functions(LPVOID lp_thread_parameter);