#include "hardware_breakpoints.h"

bool check_Dr()
{
	CONTEXT c;
	ZeroMemory(&c, sizeof(CONTEXT));
	c.ContextFlags = CONTEXT_DEBUG_REGISTERS;

	if (!GetThreadContext(GetCurrentThread(), &c)) {
		return false;
	}

	return c.Dr0 || c.Dr1 || c.Dr2 || c.Dr3;
}

DWORD WINAPI check_hardware_breakpoints(LPVOID lp_thread_parameter)
{
    while (g_hardware_breakpoints) {
        if (check_Dr) {
            std::cout << "The program is being debugged (harware breakpoints)...\n";
            ExitProcess(0);
        }
        Sleep(10000);
    }
    return 0;
}