#pragma once

#include <iostream>
#include <windows.h>

inline bool g_hardware_breakpoints = true;

DWORD WINAPI check_hardware_breakpoints(LPVOID lp_thread_parameter);