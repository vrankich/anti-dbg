#include "software_breakpoint.h"

bool check_for_INT3(PVOID ptr_func, SIZE_T nMemorySize = 0)
{
    PBYTE ptr_bytes = (PBYTE)ptr_func; 
    
    for (SIZE_T i = 0; ; i++)
    {
        /* Break on RET (0xC3) instruction
         * RET - return from procedure */
        if (ptr_bytes[i] == 0xC3) {
            break;
        }
        /* Check for INT3 (0xCC) instruction */
        if (ptr_bytes[i] == 0xCC) {
            return true;
        }
    }
    
    return false;
}

bool check_software_breakpoint(PVOID func)
{
    //auto func = function_to_check;
    //return check_for_INT3(&func);
    return check_for_INT3(func);
}

/*
    if (is_debugged()) {
        std::cout << "The program is being debugged! (0xCC)\n";
        return -1;
    }
*/