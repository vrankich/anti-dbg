#pragma once

#include <windows.h>
#include <basetsd.h>
#include <zlib.h>
#include <inttypes.h>

bool check_software_breakpoint(PVOID func);